
import random

def generate_random_number():
    return random.random()

# Example usage
random_number = generate_random_number()
print("Random number:", random_number)
